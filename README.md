1. Clone the repository

2. Do npm install

That's all you are good to use the application

**Application description**
This app will add, remove, list & read a particular note in the notes.json which is available in the project through the command line arguments.

**Add:** 
Add command require title and body of the note to be added

you can execute something like below to get the notes addded.
**command:** node app.js add --title="Give your own title" --body="Body of the note"

**Remove**
Remove need the title of the note to be removed.

**command:** node app.js add --title=**command:**

**List**
List will actually list the notes available in notes.js

**command:** node app.js list

**Read**
Read will take the title and read the content available in the notes under that title.

**command:** node app.js read --title=**command:**
